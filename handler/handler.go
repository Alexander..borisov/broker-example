package handler

import (
	"gitlab.com/Alexander..borisov/broker-example/broker"
	"log/slog"
)

const (
	UserCreateEvent broker.EventType = "user.created"
)

func NewUserCreateHandler(logger *slog.Logger) broker.Handler {
	return func(event broker.Event) error {
		logger.Info("UserCreateHandler called", "data", event.Data.(string))

		return nil
	}
}
