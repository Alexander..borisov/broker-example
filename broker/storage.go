package broker

type Storage interface {
	Persist(Event) error
	Delete(Event) error
}

// DefaultStorage returns the default event storage implementation.
// TODO implement storage
func DefaultStorage() (Storage, error) {
	return &persistentStorage{}, nil
}

type persistentStorage struct{}

func (s *persistentStorage) Persist(e Event) error {
	// Marshal event to json.
	// Insert into database table with four columns: data (jsonb), type (string), created_at (timestamp), id (hash of data).
	return nil
}

func (s *persistentStorage) Delete(e Event) error {
	// Delete event from database table using id (hash of data).
	return nil
}
