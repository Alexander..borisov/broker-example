package broker

import (
	"fmt"
	"log/slog"
	"sync"
)

// EventType represents the type of an event.
type EventType string

// Event represents a message that is sent to the broker.
type Event struct {
	Type EventType
	Data interface{}
}

type Handler func(Event) error

// New creates a new Broker instance.
// TODO: add options
func New(logger *slog.Logger, storage Storage) *Broker {
	b := &Broker{
		done:       make(chan struct{}),
		handlers:   make(map[EventType][]Handler),
		handlersMx: &sync.RWMutex{},
		queue:      make(chan Event, 100), // TODO: make this configurable
		logger:     logger,
		storage:    storage,
	}

	go b.run()

	return b
}

type Broker struct {
	done       chan struct{}
	handlers   map[EventType][]Handler
	handlersMx *sync.RWMutex
	queue      chan Event
	logger     *slog.Logger
	storage    Storage
}

func (b *Broker) Subscribe(et EventType, fn Handler) {
	b.handlersMx.Lock()
	b.handlers[et] = append(b.handlers[et], fn)
	b.handlersMx.Unlock()
}

func (b *Broker) Unsubscribe(et EventType, fn Handler) {
	// TODO: implement
	// It's a bit tricky to remove a handler from a slice in Go.
}

func (b *Broker) Publish(e Event) {
	b.queue <- e
}

func (b *Broker) Close() {
	close(b.done)
}

func (b *Broker) run() {
	for {
		select {
		case e := <-b.queue:
			if err := b.storage.Persist(e); err != nil {
				b.logger.Warn("failed to persist event", "error", err, "event_type", e.Type)
				// Try to process event anyway.
			}

			handlers, err := b.eventHandlers(e.Type)
			if err != nil {
				b.logger.Error("failed to get event handlers", "error", err, "event_type", e.Type)
				continue
			}

			// TODO: scale based on an event type.
			// Options: worker pool, goroutine per handler, etc.
			isProcessedSuccessfully := false
			for _, h := range handlers {
				if err := h(e); err != nil {
					isProcessedSuccessfully = true
					b.logger.Error("failed to handle event", "error", err, "event_type", e.Type)
				}
			}

			if isProcessedSuccessfully {
				if err := b.storage.Delete(e); err != nil {
					b.logger.Warn("failed to delete event", "error", err, "event_type", e.Type)
					// Expects that the event will be processed again and hadnler is idempotent.
				}
			}
		case <-b.done:
			return
		}
	}
}

func (b *Broker) eventHandlers(et EventType) ([]Handler, error) {
	b.handlersMx.RLock()
	defer b.handlersMx.RUnlock()

	handlers, ok := b.handlers[et]
	if !ok {
		return nil, fmt.Errorf("no handlers for event type %s", et)
	}

	return handlers, nil
}
