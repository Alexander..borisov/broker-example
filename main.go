package main

import (
	"gitlab.com/Alexander..borisov/broker-example/broker"
	"gitlab.com/Alexander..borisov/broker-example/handler"
	"log/slog"
	"time"
)

func main() {
	logger := slog.Default()
	storage, _ := broker.DefaultStorage()
	b := broker.New(logger, storage)

	b.Subscribe(handler.UserCreateEvent, handler.NewUserCreateHandler(logger))
	b.Publish(broker.Event{
		Type: handler.UserCreateEvent,
		Data: "user#1",
	})

	b.Publish(broker.Event{
		Type: "unknown",
		Data: "kurwa",
	})

	time.Sleep(time.Second)
}
